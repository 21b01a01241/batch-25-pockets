\documentclass[14pt]{beamer}
\usepackage{inputenc}
\usepackage{graphics}
\title{\LARGE POCKETS}
\date{January 28,2023}
\author[svecw]{\footnotesize 1.K Mahalakshmi - 21B01A0124 - Civil \\  2.K Manasa - 21B01A0128 - Civil \\  3.G Ishwarya - 21B01A0551 - CSE\\  4.A Neeharika - 21B01A0406 - ECE \\  5.M Rishitha - 21B01A0463 - ECE }
\usefonttheme{serif}
\usepackage{bookman}
\usepackage{hyperref}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usecolortheme{orchid}
\beamertemplateballitem

\begin{document}
    \begin{frame}
        \titlepage
    \end{frame}
    \begin{frame}{Problem statement}
        \begin{itemize}
            \item Origami, or the art of paper folding, often makes use of â€œpocketsâ€ of paper, particularly in complicated models made from multiple pieces of paper where a tab on one piece of paper needs to fit into a pocket in another piece of paper.
            \item In this problem you must count pockets in a flat folded square of paper. A pocket is defined as any opening (lying between two surfaces of paper) that is accessible from the boundary of the folded piece of paper.
        \end{itemize}        
\end{frame}
    \begin{frame}{}
\begin{figure}[htbp]
\code{\includegraphics[scale=.3]{figure1.jpeg}}
\label{fig}
\end{figure}
 \end{frame}
 \begin{frame}{Problem Description}
        \tiny Assume the paper is initially lying on a flat surface and is never completely lifted from the surface. All folds willbe horizontal or vertical. Fold lines will fall only along equally-spaced crease lines, N in each direction. On the original unfolded square, creases and edges are numbered from top to bottom and from left to right as shown in Figure 2. Each fold reduces the boundary of the folded piece of paper to a smaller rectangle; the final fold results in a square one unit in each direction. Folds are described using a crease line and a direction. For instance, â€˜2 Uâ€™ means to fold the bottom edge up using horizontal crease 2; â€˜1 Lâ€™ means to
        fold the right edge to the left using crease 1. (See Figure 2.) After several folds, creases may be aligned (for instance, creases 1 and 3 in Figure 2). Either numbermay be used to specify a fold along that line (so, in Figure 2, â€˜1 Dâ€™ and â€˜3 Dâ€™ are equivalent instructions after the first fold). Pockets are to be counted for the boundary of the final one-unit square. Once a crease is made it cannot be undone. All creases go through every layer of paper from top to bottom; disregard paper thickness.

    \end{frame}
    \begin{frame}{}
\begin{figure}[htbp]
\code{\includegraphics[scale=.3]{figure2.jpeg}}
\label{fig}
\end{figure}
 \end{frame}
 
    
    \begin{frame}
    \frametitle{Approach}
    \begin{itemize}
        \item Define the problem.
        \item Generate alternate solutions.
        \item Evaluate and select an alternative.
        \item Implement and follow up the code.
        \item We've used loops and approached through List using built in methods.
        \item We've approached through built in method append().
    \end{itemize}
    \end{frame}
    \begin{frame}{Learning}
    \begin{itemize}
        \item We Learned how to extract data from webpages by webscraping.
        \item We learned about different libraries.
        \item Working on a latex environment.
        \item Working on gitlab.
        \item We learned how to analyze the projects.
    
    \end{itemize}
    \end{frame}
    \begin{frame}{ challenges}
        \begin{itemize}
            \item Finding out best algorithm needed.
            \item Checking for different datasets in order to get best accuracy.
            \item Code efficiency.
            \item Training the model took lot of time.
            \item Accuracy of the models-overcame by rectifing our mistakes in training part.
    \end{itemize}
    \end{frame}
    \begin{frame}{Statistics}
        \begin{itemize}
               \item  Number of lines of codes-37\\
               \item Number of functions: 1 def
    \end{itemsize}
     \end{frame}       
    \begin{frame}{code}
\begin{figure}[htbp]
\code{\includegraphics[scale=.4]{code1.jpg}}
\label{fig}
\end{figure}
 \end{frame}
 \begin{frame}{code}
\begin{figure}[htbp]
\code{\includegraphics[scale=.4]{code2.jpg}}
\label{fig}
\end{figure}
 \end{frame}
     \begin{frame}{}
\begin{figure}[htbp]
\code{\includegraphics[scale=.7]{thankyou.jpg}}
\label{fig}
\end{figure}
 \end{frame}
    
\end{document}
